#pragma once
#include "SecuritySet.h"
class RefreshKey;
#include "RefreshKey.h"
#include <gmpxx.h>




class PrivateKey{
private:
    mpz_class key;
    SecuritySet * securitySet;
public:
    PrivateKey(mpz_class key, SecuritySet * securitySet);
    PrivateKey(mpz_class key);
    
    mpz_class getKey();
    SecuritySet * getSecuritySet();
    
    void setKey(mpz_class key);


};