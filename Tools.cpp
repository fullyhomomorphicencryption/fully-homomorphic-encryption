#include "Tools.h"


mpz_class Tools::random(int bits, int even) {
    
    mpz_class randNumber;
    mpz_class randSize(bits);
    gmp_randclass rr(gmp_randinit_default);
    random_device rd;
    unsigned long int seed = rd();
    rr.seed(seed);
    randNumber = rr.get_z_bits(randSize);
    
    if(randNumber%2 == even){
        randNumber -= 1;
    }
    
    return randNumber;
    
    
    
}

