#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <errno.h>
#include <iostream>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>

class Client {
	public:
		Client(char* address, unsigned int port_number, char* file_received);
		~Client();

		void receive_sequence();

	private:
		unsigned int port_number;
		char* server_address;
		char* file_received;

		int stream_socket;
		struct sockaddr_in remote_addr;
};