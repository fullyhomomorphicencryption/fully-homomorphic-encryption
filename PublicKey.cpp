#include "PublicKey.h"
#include "FHE.h"

PublicKey::PublicKey(vector<RawData*>* publicKey, SecuritySet* securitySet) {
    this->publicKey = publicKey;
    this->securitySet = securitySet;
}
PublicKey::PublicKey(vector<RawData*>* publicKey) {
    this->publicKey = publicKey;
}

mpz_class PublicKey::getMpzPublicKey() {
    this->getPublicKey()->getData();
}

RawData* PublicKey::getPublicKey() {
    srand(time(NULL));
    vector <RawData*>* subset = new vector<RawData*>();

    while (subset->size() < 1) {
        for (int i = 0; i < publicKey->size(); i++) {
            if (rand() % 2 == 1) {
                subset->push_back(publicKey->at(i));
            }
        }
    }

    RawData * value = subset->at(0);
    for (int i = 1; i < subset->size(); i++) {
        RawData * pk = subset->at(i);
        value = FHE::XOR(value, pk);
    }
    return value;
}

vector<RawData*>* PublicKey::getFullPublicKey() {
    return this->publicKey;
}

SecuritySet* PublicKey::getSecuritySet() {
    return this->securitySet;
}