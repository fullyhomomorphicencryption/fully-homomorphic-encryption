#include <algorithm>

#include "SecuritySet.h"
#include <iostream>

using namespace std;

SecuritySet::SecuritySet(int security) {
    if (security <= 0){
        throw invalid_argument("your security parameter is wrong");
    }

    this->keySize = pow(security,5);
    this->saltSize = pow(security,2);
    this->noiseSize = security;
    this->publicKeyLength = security*2;//figure out how to do it in proper way
    this->security = security;
    

}

SecuritySet::SecuritySet(int keySize, int noiseSize, int saltSize,int publicKeyLength) {
    if(keySize == 0 || noiseSize >= keySize || publicKeyLength <= 0){
        cout << "wrong ss" << endl;
        throw invalid_argument("your security parameters are wrong");
    }
    this->keySize = keySize;
    this->noiseSize = noiseSize;
    this->saltSize = saltSize;
    this->publicKeyLength = publicKeyLength;
}

int SecuritySet::getKeySize() {
    return this->keySize;
}

int SecuritySet::getNoiseSize() {
    return this->noiseSize;
}

int SecuritySet::getSaltSize() {
    return this->saltSize;
}

int SecuritySet::getPublicKeyLength() {
    return this->publicKeyLength;
}

int SecuritySet::getSubsetLength() {
    return this->subsetLength;
}

