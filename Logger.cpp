#include "Logger.h"
//#include <curl/curl.h>

Logger * Logger::instance = 0;

Logger::Logger() {
    this->nbAnd = 0;
    this->nbXor = 0;
    this->nbOr = 0;
    this->nbRefresh = 0;

    this->bitNbAnd = 0;
    this->bitNbXor = 0;
    this->bitNbOr = 0;
    this->bitNbRefresh = 0;

    this->nbUpdate = 0;

    // Create log file
    this->bitLogFile.open("bits.log", ios::trunc);

    // Create trace log file
    this->bitTracelogFile.open("bit-trace.log");
    bitTracelogFile << "# and xor or Refresh exhaustion diff\n";


    this->currentData = NULL;

}

Logger::~Logger() {
    this->bitLogFile.close();
    this->bitTracelogFile.close();
}

void Logger::newOperation(OPERATION o) {
    if (o == OPERATION::AND) {
        nbAnd++;
    } else if (o == OPERATION::XOR) {
        nbXor++;
    } else if (o == OPERATION::OR) {
        nbOr++;
    } else if (o == OPERATION::REFRESH) {
        nbRefresh++;
    }
    //  update();
}

void Logger::newCurrentBitOperation(OPERATION o) {
    if (o == OPERATION::AND) {
        bitNbAnd++;
    } else if (o == OPERATION::XOR) {
        bitNbXor++;
    } else if (o == OPERATION::OR) {
        bitNbOr++;
    } else if (o == OPERATION::REFRESH) {
        bitNbRefresh++;
    }
    update();
}

void Logger::setCurrentData(RawData* data) {
    this->currentData = data;
}

void Logger::getHistogram() {

}

void Logger::send() {
    float diff = (this->currentData->getKeyLength() - this->currentData->getNoiseLength());
    float exhaustion = this->currentData->getNoiseLength();
    string cmd = "curl -X PATCH -d '{\"and\":" + to_string(nbAnd) + ",\"xor\":" + to_string(nbXor) + ",\"refresh\":" + to_string(nbRefresh) + ",\"exhaustion\":" + to_string(exhaustion) + "}' \
    https://fhe-display.firebaseio.com/bit/.json";
    system(strdup(cmd.c_str()));
}

Logger* Logger::getInstance() {
    if (instance == 0) {
        instance = new Logger();
        instance->bits = new vector<RawData*>();
    }
    return instance;
}

void Logger::addBit(RawData* bit) {
    this->bits->push_back(bit);
    update();
}

void Logger::update() {

    if (this->currentData != NULL) {

        RawData* bit = static_cast<RawData*> (this->currentData);
        if (bit != NULL && bit->isTraced() && bit->isCiphered()) {

            // Followed bit:
            float diff = (bit->getKeyLength() - bit->getNoiseLength());
            float exhaustion = (diff / (float) bit->getKeyLength())*100;
            this->bitTracelogFile << this->nbUpdate << " " << nbAnd << " " << nbXor << " " << nbOr << " " << nbRefresh << " " << exhaustion << " " << diff << "\n";
            this->nbUpdate++;
            if (sendData) {
                send();
            }

        }

    } else {

        // all data
        this->bitLogFile.open("bits.log", ios::trunc);
        this->bitLogFile << "{\n\"and\": " << nbAnd << ",\n\"xor\": " << nbXor << ",\n\"or\": " << nbOr << ",\n\"refresh\": " << nbRefresh << ",\n\"nbBits\": " << bits->size() << "\n}\n";
        this->bitLogFile.close();
    }


}

bool Logger::isSendData() const {
    return sendData;
}

void Logger::setSendData(bool sendData) {
    this->sendData = sendData;
}
