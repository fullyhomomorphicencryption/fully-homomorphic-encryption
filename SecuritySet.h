#include "OPERATIONS.h"
#include <vector>
#include <stdexcept>
#include "math.h"
#pragma once


using namespace std;

class SecuritySet{
private:
    int keySize;
    int noiseSize;
    int saltSize;
    int publicKeyLength;
    int subsetLength;
    int security;
    
public:
    SecuritySet(int security);
    SecuritySet(int keySize,int noiseSize, int saltSize,int publicKeyLength);
    
    int getKeySize();
    int getNoiseSize();
    int getSaltSize();
    int getPublicKeyLength();
    int getSubsetLength();
};