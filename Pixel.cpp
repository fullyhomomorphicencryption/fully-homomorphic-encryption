/* 
 * File:   Pixel.cpp
 * Author: romain
 * 
 * Created on 2 février 2015, 15:35
 */

#include "Pixel.h"

Pixel::Pixel():ComplexData() {
}

Pixel::Pixel(Pixel* pixel):ComplexData() {
    this->r = new Byte(pixel->getR());
    this->g = new Byte(pixel->getG());
    this->b = new Byte(pixel->getG());
}

Pixel::Pixel(int r, int g, int b):ComplexData(){
    this->r = new Byte(r);
    this->g = new Byte(g);
    this->b = new Byte(b);
}

Pixel::Pixel(Byte* r, Byte* g, Byte* b):ComplexData(){
    this->r = r;
    this->g = g;
    this->b = b;
}

Pixel::~Pixel() {
    delete this->r;
    delete this->g;
    delete this->b;
}



vector<DataObject*>* Pixel::getData() {
    vector<DataObject*>* colors = new vector<DataObject*>();
    colors->push_back(this->r);
    colors->push_back(this->g);
    colors->push_back(this->b);
    
    return colors;
}
