#pragma once
#include "KeySet.h"
#include "SecuritySet.h"
#include "PrivateKey.h"
#include "PublicKey.h"
#include "DataObject.h"
#include "Tools.h"
#include "ComplexData.h"
#include "MPZ.h"

using namespace std;

class FHE{
private:
    
public:
    static KeySet * generateKeySet(SecuritySet * parameters, string saveDirectory = "");
    
    static mpz_class mpzSymetricEncrypt(PrivateKey * privateKey, mpz_class plainData);
    static mpz_class mpzSymetricDecrypt(PrivateKey * privateKey, mpz_class cipheredData);
    static mpz_class mpzAsymetricEncrypt(PublicKey * publicKey, mpz_class plainData);
    
    static void symetricEncrypt(PrivateKey * privateKey,RefreshKey * refreshKey, DataObject* plainData);
    static void symetricDecrypt(PrivateKey * privateKey, DataObject* cipheredData);
    static void symetricHomomorphicDecrypt(vector<MPZ*>* zi, MPZ* si, RawData* cipheredData);
    static void asymetricEncrypt(PublicKey * publicKey, RefreshKey * refreshKey, DataObject* plainData);

    
    static void asymetricRefreshableEncrypt(KeySet * ks, DataObject* plainData);
    static void asymetricRefreshableEncrypt(RefreshKey * rk, DataObject* plainData);
    
    static void refresh(RawData * toRefresh);
    static void fullyHomomorphicRefresh(PublicKey * pk2, MPZ* toRefresh);
    static void fullyHomomorphicRefresh(DataObject* toRefresh);
    
    static vector<MPZ*>* genZi(RawData* toEncrypt, int precision);
    static MPZ* genRefreshPrivateKey(PrivateKey * sk, int precision);
    static MPZ * genEncryptedRefreshPrivateKey(KeySet * ks, int precision);
    
    static RawData * XOR(RawData * a, RawData * b);
    static RawData * AND(RawData * a, RawData * b);
    
    
};