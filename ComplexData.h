#pragma once
#include <vector>

#include "DataObject.h"
using namespace std;

class ComplexData : public DataObject {
public:
    ComplexData();
    virtual vector<DataObject*>* getData() = 0;



};