#pragma once

class DataObject {
public:
    DataObject(bool complex);
    DataObject(bool complex, bool traced);

    ~DataObject();

    bool isComplex();
    bool isTraced();
    void setComplex(bool complex);
    void setTraced(bool traced);
    


private:
   bool complex;
};