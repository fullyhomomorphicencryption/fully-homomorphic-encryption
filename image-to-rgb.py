import numpy as np
from PIL import Image
img = Image.open('key.bmp')
arr = np.array(img) 
f1=open('./image.rgb', 'w+')
result = ""

imgSize = 32 # Square image size

for i in range(0,imgSize*imgSize):
    f1.write(str(arr[i/imgSize, i%imgSize][0]) + "\n" + str(arr[i/imgSize, i%imgSize][1]) + "\n" + str(arr[i/imgSize, i%imgSize][2]) + "\n")
