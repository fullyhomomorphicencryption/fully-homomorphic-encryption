#pragma once
#include <gmpxx.h>
#include "ComplexData.h"
#include "RawData.h"

class Byte : public ComplexData{
    public:
        Byte();
        Byte(Byte*);
        Byte(char data);
        Byte(int data);
        Byte(vector<DataObject*>*);
        
        ~Byte();
        
        vector<DataObject*>* getData();
        
        Byte * add(Byte*);
        Byte * leftShift(int times);
        Byte * rightShift(int times);
        Byte * substract(Byte*);
        char getChar();
        int getInt();
    private:
        vector<DataObject*>* bits;
};