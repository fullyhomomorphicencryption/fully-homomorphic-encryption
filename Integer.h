#pragma once
#include <gmpxx.h>
#include "ComplexData.h"
#include "RawData.h"

class Integer : public ComplexData{
    public:
        Integer();
        Integer(Integer*);
        Integer(int data);
        Integer(vector<DataObject*>* bits);
        
        ~Integer();
        
        vector<DataObject*>* getData();
        
        Integer * add(Integer*);
        Integer * leftShift(int times);
        Integer * rightShift(int times);
        Integer * substract(Integer*);
        int getInt();
    private:
        vector<DataObject*>* bits;
};