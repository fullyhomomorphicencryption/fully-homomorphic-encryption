#include <stdio.h>

#include "Server.h"

#define PORT_NUMBER     5000
#define SERVER_ADDRESS  "127.0.0.1"
#define FILE_TO_SEND    "nebuleuse.jpg"

int main() {
	Server* server = new Server(SERVER_ADDRESS, PORT_NUMBER, FILE_TO_SEND);
	server->listen_sequence();
	return 1;
}