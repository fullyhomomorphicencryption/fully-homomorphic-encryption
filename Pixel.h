#pragma once
#include "ComplexData.h"
#include "Byte.h"


class Pixel : public ComplexData {
public:
    Pixel();
    Pixel(Pixel*);
    Pixel(int r, int g, int b);
    Pixel(Byte* r, Byte* g, Byte* b);
    
    ~Pixel();
    
    Byte* getB() const {
        return b;
    }

    void setB(Byte* b) {
        this->b = b;
    }

    Byte* getG() const {
        return g;
    }

    void setG(Byte* g) {
        this->g = g;
    }

    Byte* getR() const {
        return r;
    }

    void setR(Byte* r) {
        this->r = r;
    }

private:
    Byte* r;
    Byte* g;
    Byte* b;
    virtual vector<DataObject*>* getData();
};