#include "RawData.h"
#include "RefreshKey.h"


RawData::RawData(mpz_class data, bool ciphered,unsigned int keylength,unsigned int noiseLength) : DataObject(false){
    this->data = data;
    this->ciphered = ciphered;
    this->keyLength = keylength;
    this->noiseLength = noiseLength;
    Logger::getInstance()->addBit(this);
}

RawData::RawData(RawData * rawData) : DataObject(false){
    this->data = mpz_class(rawData->data);
    this->ciphered = rawData->ciphered;
    this->keyLength = rawData->keyLength;
    this->noiseLength = rawData->noiseLength;
    this->refreshKey = rawData->getRefreshKey();
    Logger::getInstance()->addBit(this);
}

RawData::RawData(mpz_class data, bool ciphered):DataObject(false){
    this->data = data;
    this->ciphered = ciphered;
    this->keyLength = 0;
    this->noiseLength = 0;
    this->refreshKey = 0;
    Logger::getInstance()->addBit(this);
}

RawData::~RawData(){
    Logger::getInstance()->addBit(this);
}

bool RawData::isCiphered() {
    return ciphered;
}

mpz_class RawData::getData() {
    return data;
}

unsigned int RawData::getKeyLength() {
    return keyLength;
}

unsigned int RawData::getNoiseLength() {
    return noiseLength;
}


void RawData::setCiphered(bool ciphered) {
    this->ciphered = ciphered;
}

void RawData::setData(mpz_class data) {
    this->data = data;
}


void RawData::setKeyLength(unsigned int len) {
    this->keyLength = len;
}

void RawData::setNoiseLength(unsigned int len) {
    this->noiseLength = len;
}

RefreshKey* RawData::getRefreshKey() {
    return this->refreshKey;
}

void RawData::setRefreshKey(RefreshKey* key) {
    this->refreshKey = key;
}

void RawData::copyValues(RawData* other){

    this->data = other->getData();
    this->ciphered = other->isCiphered();
    this->keyLength = other->getKeyLength();
    this->noiseLength = other->getNoiseLength();

  //  this->setTraced(other->isTraced());

    this->refreshKey = other->getRefreshKey();

}

void RawData::trace(RawData* next,OPERATION operation) {

    if(this->isTraced()){
        if(next != this){
            next->setTraced(true);
            this->setTraced(false);
        }

        Logger::getInstance()->newOperation(operation);

        Logger::getInstance()->setCurrentData(next);
        Logger::getInstance()->newCurrentBitOperation(operation);

    }
}

bool RawData::isTraced() {
    return this->traced;
}

void RawData::setTraced(bool traced) {
    this->traced = traced;
    Logger::getInstance()->setCurrentData(this);
}
