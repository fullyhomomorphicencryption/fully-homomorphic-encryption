#pragma once
class Logger;
#include "RawData.h"
class RawData;
#include <fstream>
#include <vector>

using namespace std;

class Logger{
private:
    static Logger * instance;
    
    Logger();
    
public:
    
    ~Logger();
    bool sendData = false;
    int nbAnd;
    int nbXor;
    int nbOr;
    int nbRefresh;
    
    int bitNbAnd;
    int bitNbXor;
    int bitNbOr;
    int bitNbRefresh;
    
    int nbUpdate;
    
    vector<RawData *>* bits;
    RawData * currentData;
    
    ofstream bitLogFile;
    ofstream bitTracelogFile;
    
  
    
    void setCurrentData(RawData *);
    void newOperation(OPERATION);
    void newCurrentBitOperation(OPERATION o);
    void getHistogram();
    void send();
    void addBit(RawData *);
    static Logger* getInstance();
    void update();

    bool isSendData() const;
    void setSendData(bool sendData);

};