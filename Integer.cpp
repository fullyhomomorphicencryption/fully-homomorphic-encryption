#include "Integer.h"
#include "FHE.h"

Integer::Integer():ComplexData() {

}

Integer::Integer(Integer * integer):ComplexData(){
    this->setComplex(integer->isComplex());
    this->bits = new vector<DataObject*>();
    RawData* toPush;
    for (int k = 0; k < 32; k++) {
      toPush = new RawData(static_cast<RawData*> (integer->bits->at(k)));
      this->bits->push_back(toPush);
    }
}

Integer::Integer(int data):ComplexData(){
    this->bits = new vector<DataObject*>();
    RawData* toPush;
    for (int k = 0; k < 32; k++) {
        mpz_class n((data & ( 1 << k )) >> k);
        toPush = new RawData(n, false);
        this->bits->push_back(toPush);
    }
}

Integer::Integer(vector<DataObject*>* bits):ComplexData() {
    this->bits = bits;
    
}

Integer::~Integer() {
  for(int i=0;i<this->bits->size();i++){
      if(this->bits->at(i) != NULL){
         delete this->bits->at(i);
         this->bits->at(i) = NULL;
      }
  }
  delete this->bits;
}

vector<DataObject*>* Integer::getData() {
    return this->bits;
}

int Integer::getInt(){
    RawData* bit;
    mpz_class value;
    int result = 0;
    for(int i = 0; i < 32; i++){
        
        bit = static_cast<RawData*> (this->bits->at(i));
        value = bit->getData();
        if(!bit->isCiphered() && value == 1){
            result = result | (1 << i);
        }
    }
    return result;
}

Integer* Integer::add(Integer* other) {
    
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * a;
    RawData * b;
    RawData * c;
    RawData * Cin;
    RawData * out;
    
    RawData * tmpa = static_cast<RawData*> (this->getData()->at(0));
    RawData * tmpb = static_cast<RawData*> (other->getData()->at(0));
    
    Cin = FHE::AND(tmpa,tmpb);
    out = FHE::XOR(tmpa, tmpb);
    result->push_back(out);
    
     
    for(int i=1;i<32;i++){
        
        tmpa = static_cast<RawData*> (this->getData()->at(i));
        tmpb = static_cast<RawData*> (other->getData()->at(i));
        
        a = FHE::XOR(tmpa, tmpb);
        b = FHE::AND(tmpa, tmpb);
        c = FHE::AND(a, Cin);
        out = FHE::XOR(a, Cin);
        Cin = FHE::XOR(c, b);
        result->push_back(out);
    }
    
    return new Integer(result);
}


Integer* Integer::leftShift(int times = 1){
    
    // TODO : throw times>32
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * tmp;
    
    for(int j = 0 ; j<times ; j++){
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }
     

    for(int i=0;i<32-times;i++){
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }
    
    return new Integer(result);
}

Integer* Integer::rightShift(int times = 1){
    
    // TODO : throw times>32
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * tmp;
     
    for(int i=times ; i<32 ; i++){
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }
    
    for(int j = 0 ; j<times ; j++){
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }
    
    return new Integer(result);
}


Integer* Integer::substract(Integer* other){
    vector<DataObject*>* result = new vector<DataObject*>();
}
