#include "KeySet.h"

KeySet::KeySet(PrivateKey* privateKey, PublicKey* publicKey, RefreshKey * refreshKey) {
    this->privateKey = privateKey;
    this->publicKey = publicKey;
    this->refreshKey = refreshKey;
}

RefreshKey* KeySet::getRefreshKey() {
    return this->refreshKey;
}

void KeySet::setRefreshKey(RefreshKey* refreshKey){
    this->refreshKey = refreshKey;
}

PrivateKey* KeySet::getPrivateKey() {
    return this->privateKey;
}

PublicKey* KeySet::getPublicKey() {
    return this->publicKey;
}

void KeySet::setPrivateKey(PrivateKey* privateKey) {
    this->privateKey = privateKey;
}

void KeySet::setPublicKey(PublicKey* publicKey) {
    this->publicKey = publicKey;
}

void KeySet::exportToFile(string path) {
    ofstream exportFile;
    exportFile.open(path);
    exportFile << this->privateKey->getKey();
    exportFile << endl;
    PublicKey * pk = this->getPublicKey();
    exportFile << pk->getPublicKey()->getData();
    exportFile << endl;
    exportFile.close();
}

void KeySet::exportPk(string path) {
    ofstream exportFile;
    exportFile.open(path);
    vector<RawData*>* list = this->publicKey->getFullPublicKey();
    for (RawData* rd : *list) {
        exportFile << rd->getData() << endl;
    }
    exportFile.close();
}

void KeySet::exportSk(string path) {
    ofstream exportFile;
    exportFile.open(path);
    exportFile << this->privateKey->getKey();
    exportFile << endl;
    exportFile.close();
}

void KeySet::importSk(string path) {
    string line;
    ifstream importFile(path);
    if (importFile.is_open()) {
        int i = 0;
        while (getline(importFile, line)) {
            if (i == 0) {
                this->privateKey = new PrivateKey(mpz_class(line));
                //cout << "imported sk" << endl;
            }
            i++;
        }
        importFile.close();
    } else cout << "Unable to open file";

}

void KeySet::importPubKey(string path, SecuritySet * ss) {
    string line;
    ifstream importFile(path);
    if (importFile.is_open()) {
        vector<RawData*>* pubKey = new vector<RawData*>;
        while (getline(importFile, line)) {
            RawData * toPush = new RawData(mpz_class(line), true, ss->getKeySize(), ss->getNoiseSize());
            pubKey->push_back(toPush);
        }

        this->setPublicKey(new PublicKey(pubKey));
        importFile.close();
    } else {
        cout << "Unable to open file" << endl;
    };

}

void KeySet::importKeySet(string path, SecuritySet * ss) {

    string line;
    ifstream importFile(path);
    if (importFile.is_open()) {
        int i = 0;
        while (getline(importFile, line)) {
            if (i == 0) {
                this->setPrivateKey(new PrivateKey(mpz_class(line)));
            }
            if (i == 1) {
                MPZ curr = MPZ(mpz_class(line));
                vector<RawData*>* pubKey = new vector<RawData*>;
                for (DataObject *ob : *curr.getData()) {
                    RawData * rd = static_cast<RawData*> (ob);
                    RawData * toPush = new RawData(rd->getData(), false);
                    pubKey->push_back(static_cast<RawData*> (ob));
                }
                this->setPublicKey(new PublicKey(pubKey));
            }
            i++;
        }
        importFile.close();
    } else cout << "Unable to open file";

}