#pragma once
#include <gmpxx.h>
#include "ComplexData.h"
#include "RawData.h"

class MPZ : public ComplexData{
    public:
        MPZ();
        MPZ(MPZ*);
        MPZ(mpz_class mpz);
        MPZ(vector<int> bits, int size);
        MPZ(RawData * rd, MPZ* refreshKey);
        MPZ(vector<DataObject*>* bits, int size);
        
        ~MPZ();
        
        vector<DataObject*>* getData();
        
        int getSize() const;
        void setSize(int size);
        void setBits(vector<DataObject*>* bits);
        MPZ* getEncryptedSecretKey();
        void setEncryptedSecretKey(MPZ* encryptedSecretKey);
        bool isRefreshable();
        void setRefreshable(bool isRefreshable);
        
        
//      MPZ * add(MPZ*);
        mpz_class getMPZ();
        string toString();
        MPZ* leftShift(int times = 1);
        MPZ* rightShift(int times = 1);
        MPZ* leftPadding(int times = 1);
        MPZ* rightPadding(int times = 1);
        MPZ* leftCrop(int times = 1);
        MPZ* rightCrop(int times = 1);
        MPZ* add(MPZ* other);
        MPZ* addWithoutCarry(MPZ* other);
        
        
    private:
        vector<DataObject*>* bits;
        int size;
        bool refreshable = false;
        MPZ* encryptedSecretKey;
};