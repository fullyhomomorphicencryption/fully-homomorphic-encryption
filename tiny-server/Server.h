#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>
#include <errno.h>
#include <string.h> //for memset
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>

class Server {
	public:
		Server(char* address, unsigned int port_number, char* file_to_serve);
		~Server();

		void listen_sequence();
		
	private:
		unsigned int port_number;
		char* address;
		char* file_to_serve;

		int stream_socket;
		struct sockaddr_in      server_addr;
        struct sockaddr_in      peer_addr;
};