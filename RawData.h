#pragma once

class KeySet;
#include "DataObject.h"
#include "OPERATIONS.h"
#include <gmpxx.h>
#include "Logger.h"
#include "RefreshKey.h"
#include "KeySet.h"

using namespace std;

class RawData : public DataObject{
public:
    RawData(mpz_class data, bool ciphered,unsigned int keylength,unsigned int noiseLength);
    RawData(mpz_class data, bool ciphered);
    RawData(RawData *);
    
    void setData(mpz_class data);
    void setCiphered(bool ciphered);
    void setNoiseLength(unsigned int );
    void setKeyLength(unsigned int );
    
    mpz_class getData();
    bool isCiphered();
    unsigned int getNoiseLength();
    unsigned int getKeyLength();
    
    RefreshKey * getRefreshKey();
    void setRefreshKey(RefreshKey * key);
    
    
    
    void copyValues(RawData* other);
    
    void trace(RawData* next,OPERATION operation);
    bool isTraced();
    void setTraced(bool);
    
    ~RawData();

private:
    bool ciphered;
    mpz_class data;
    RefreshKey * refreshKey;
    
    
    bool traced;
    
    unsigned int noiseLength;
    unsigned int keyLength;
};