#include "FHE.h"
#include "MPZ.h"
#include <math.h>
#include <sstream>

#define PRECISION_FACTOR 2

KeySet* FHE::generateKeySet(SecuritySet * parameters, string saveDirectory) {
    //generate private key;
    mpz_class mpzPrivateKey = Tools::random(parameters->getKeySize(), false);
    PrivateKey * privateKey = new PrivateKey(mpzPrivateKey, parameters);

    vector<RawData*>* pk = new vector<RawData *>();
    RawData * current;
    
    int precision = 2*(parameters->getKeySize()+parameters->getSaltSize());
    RefreshKey * refreshKey = new RefreshKey(NULL,privateKey,precision);
    
    for (int i = 0; i < parameters->getPublicKeyLength(); i++) {
        current = new RawData(FHE::mpzSymetricEncrypt(privateKey, 0), true, parameters->getKeySize(), parameters->getNoiseSize());
        current->setRefreshKey(refreshKey);
        pk->push_back(current);
    }
    
    
    PublicKey * publicKey = new PublicKey(pk, parameters);
    refreshKey->setPublicKey(publicKey);
    
    
    
    return new KeySet(privateKey, publicKey,refreshKey);

}

mpz_class FHE::mpzSymetricDecrypt(PrivateKey* privateKey, mpz_class data) {
    mpz_class key = privateKey->getKey();
    return data % key % 2;
}

MPZ * FHE::genRefreshPrivateKey(PrivateKey * sk, int precision) {

    vector<DataObject*>* si = new vector<DataObject *>();
    mpf_class pFloat(sk->getKey());
    // TODO : optain 1/p for an encrypted key
    mpf_class q = 1 / pFloat;
    mp_exp_t exp;
    string tmpString = q.get_str(exp, 2, precision);

    ostringstream stream;
    for (int e = 0; e < (-exp) + 1; e++) {
        stream << "0";
    }
    stream << tmpString;
    string qString = stream.str();

    for (int i = 0; i < precision; i++) {
        RawData * currentBit;
        if (i < qString.size()) {
            currentBit = new RawData(mpz_class(qString.at(i) - '0'), false);
        } else {
            currentBit = new RawData(mpz_class(0), false);
        }
        si->push_back(currentBit);
    }

    MPZ * refreshKey = new MPZ(si, precision);

    return refreshKey;
}

MPZ * FHE::genEncryptedRefreshPrivateKey(KeySet * ks, int precision) {
    // Generate si
    MPZ* refreshKey = FHE::genRefreshPrivateKey(ks->getPrivateKey(), precision);

    // encrypt sk (si's bits) with pk2
    for (DataObject * ob : *refreshKey->getData()) {
        RawData * rkeyBit = static_cast<RawData*> (ob);
        FHE::asymetricEncrypt(ks->getPublicKey(), ks->getRefreshKey(), rkeyBit);
    }

    return refreshKey;
}

vector<MPZ*>* FHE::genZi(RawData* toEncrypt, int precision) {

    MPZ* mpzObject = new MPZ(toEncrypt->getData());

    std::vector<MPZ*>* xi = new vector<MPZ *>();

    for (int m = 0; m < precision; m++) {

        MPZ * currMpz = new MPZ(mpzObject);

        if (m < mpzObject->getSize()) {
            currMpz = currMpz->leftCrop(mpzObject->getSize() - (m + 1));
        } else {
            currMpz = currMpz->leftPadding(-(mpzObject->getSize() - (m + 1)));
        }
        currMpz = currMpz->rightPadding(precision - m - 1);
        xi->push_back(currMpz);
    }

    return xi;

}

void FHE::symetricHomomorphicDecrypt(vector<MPZ*>* zi, MPZ* si, RawData* lsb) {

    if (zi->size() != si->getSize()) {
        cout << "zi'size != si'size" << endl;
        return;
    }

    // do the sum of zi's mpz
    MPZ * sum = new MPZ(mpz_class(0));
    MPZ* currentMpz;
    cout << "zi size: " << zi->size() << endl;
    for (int j = 0; j < zi->size(); j++) {
        currentMpz = zi->at(j);

        // Apply si to sum only significant zis
        for (int k = 0; k < currentMpz->getSize(); k++) {
            RawData * currentRd = static_cast<RawData*> (currentMpz->getData()->at(k));
            RawData * sii = static_cast<RawData*> (si->getData()->at(j));
            currentRd->copyValues(FHE::AND(currentRd, sii));
            currentRd->setRefreshKey(lsb->getRefreshKey());
        }
        cout << "sum z" << j << "/" << zi->size() << endl;
        sum = sum->addWithoutCarry(currentMpz);
    }
    RawData* qParity = FHE::XOR(static_cast<RawData*> (sum->getData()->at(zi->size() - 1)), static_cast<RawData*> (sum->getData()->at(zi->size() - 2)));

    //LSB(c) XOR LSB(sum)
    lsb->copyValues(FHE::XOR(lsb, qParity));

}

mpz_class FHE::mpzSymetricEncrypt(PrivateKey* privateKey, mpz_class plainData) {
    mpz_class salt = Tools::random(privateKey->getSecuritySet()->getSaltSize(), 2);
    mpz_class noise = Tools::random(privateKey->getSecuritySet()->getNoiseSize(), 1);
    return privateKey->getKey() * salt + noise + plainData;
}

mpz_class FHE::mpzAsymetricEncrypt(PublicKey* publicKey, mpz_class plainData) {
    return plainData + publicKey->getMpzPublicKey();
}

void FHE::asymetricEncrypt(PublicKey* publicKey,RefreshKey* refreshKey, DataObject* plainData) {
    if (plainData->isComplex()) {
        ComplexData * cd = static_cast<ComplexData*> (plainData);
        vector<DataObject*>* next = cd->getData();
        for (int i = 0; i < next->size(); i++) {
            FHE::asymetricEncrypt(publicKey, refreshKey, next->at(i));
        }
    } else {
        RawData * rd = static_cast<RawData*> (plainData);
        RawData * pk = publicKey->getPublicKey();
        rd->copyValues(FHE::XOR(rd, pk));
    }
}

void FHE::symetricDecrypt(PrivateKey* privateKey, DataObject* cipheredData) {
    if (cipheredData->isComplex()) {
        ComplexData * cd = static_cast<ComplexData*> (cipheredData);
        vector<DataObject*>* next = cd->getData();
        for (int i = 0; i < next->size(); i++) {
            FHE::symetricDecrypt(privateKey, next->at(i));
        }
    } else {
        RawData * rd = static_cast<RawData*> (cipheredData);
        rd->setData(FHE::mpzSymetricDecrypt(privateKey, rd->getData()));
        rd->setCiphered(false);
        rd->setKeyLength(0);
        rd->setNoiseLength(0);
    }

}

void FHE::symetricEncrypt(PrivateKey* privateKey, RefreshKey * refreshKey, DataObject* plainData) {
    if (plainData->isComplex()) {
        ComplexData * cd = static_cast<ComplexData*> (plainData);
        vector<DataObject*>* next = cd->getData();
        for (int i = 0; i < next->size(); i++) {
            FHE::symetricEncrypt(privateKey,refreshKey,next->at(i));
        }
    } else {
        RawData * rd = static_cast<RawData*> (plainData);
        mpz_class cipheredMpz = FHE::mpzSymetricEncrypt(privateKey, rd->getData());
        rd->setData(cipheredMpz);
        rd->setKeyLength(privateKey->getSecuritySet()->getKeySize());
        rd->setNoiseLength(privateKey->getSecuritySet()->getNoiseSize());
        rd->setCiphered(true);
        rd->setRefreshKey(refreshKey);
        
        
    }

}

void FHE::asymetricRefreshableEncrypt(KeySet * ks, DataObject* plainData) {
    if (plainData->isComplex()) {
        ComplexData * cd = static_cast<ComplexData*> (plainData);
        vector<DataObject*>* next = cd->getData();
        for (int i = 0; i < next->size(); i++) {
            FHE::asymetricRefreshableEncrypt(ks, next->at(i));
        }
    } else {
        RawData * rd = static_cast<RawData*> (plainData);

        rd->copyValues(FHE::XOR(rd, ks->getPublicKey()->getPublicKey()));
        // TODO change
        int cLen = rd->getData().get_str(2).size();
        int precision = cLen * PRECISION_FACTOR;
        rd->setRefreshKey(new RefreshKey(ks->getPublicKey(), FHE::genEncryptedRefreshPrivateKey(ks, precision), precision));
    }
}

void FHE::asymetricRefreshableEncrypt(RefreshKey * rk, DataObject* plainData) {
    if (plainData->isComplex()) {
        ComplexData * cd = static_cast<ComplexData*> (plainData);
        vector<DataObject*>* next = cd->getData();
        for (int i = 0; i < next->size(); i++) {
            FHE::asymetricRefreshableEncrypt(rk, next->at(i));
        }
    } else {
        RawData * rd = static_cast<RawData*> (plainData);

        rd->copyValues(FHE::XOR(rd, rk->getPublicKey()->getPublicKey()));
        // TODO change
        int cLen = rd->getData().get_str(2).size();
        int precision = cLen * PRECISION_FACTOR;
        rd->setRefreshKey(rk);
        
    }
}


void FHE::fullyHomomorphicRefresh(DataObject* toRefresh) {


    if (toRefresh->isComplex()) {
        ComplexData * cd = static_cast<ComplexData*> (toRefresh);
        vector<DataObject*>* next = cd->getData();
        for (int i = 0; i < next->size(); i++) {
            FHE::fullyHomomorphicRefresh(next->at(i));
        }
    } else {
        RawData * toRefreshRd = static_cast<RawData*> (toRefresh);
        
        
        if (!toRefreshRd->isCiphered()) {
            cout << "Can't refresh an unciphered RawData" << endl;
            return;
        }
        
        RefreshKey * rk;
        if ((rk = toRefreshRd->getRefreshKey()) == NULL) {
            cout << "Can't refresh if RefreshKey is not set" << endl;
            return;
        }

        if (toRefreshRd->getRefreshKey()->getPrivateKey() != NULL) {
            // Mode cheat
            FHE::refresh(toRefreshRd);
            return;
        }

        ///////////// Generate zi
        int precision = rk->getPrecision();
        vector<MPZ*>* zi = FHE::genZi(toRefreshRd, precision);

        MPZ * toRefreshMPZ = new MPZ(toRefreshRd, rk->getEncryptedPrivateKey());

        ///////////// Encrypt        
        for (DataObject * ob : *toRefreshMPZ->getData()) {
            RawData * rd = static_cast<RawData*> (ob);
            FHE::asymetricRefreshableEncrypt(toRefreshRd->getRefreshKey(), rd);
        }

        /////////////  Decrypt through encryption
        FHE::symetricHomomorphicDecrypt(zi, rk->getEncryptedPrivateKey(), static_cast<RawData*> (toRefreshMPZ->getData()->front()));

        toRefreshRd->setData(static_cast<RawData*> (toRefreshMPZ->getData()->front())->getData());
        
    }
    
}

void FHE::fullyHomomorphicRefresh(PublicKey * pk2, MPZ * toRefresh) {
    // toRefresh should be an encrypted MPZ created from the mpz of an encrypted RawData.

    if (toRefresh->isRefreshable()) {

        ///////////// Generate zi
        int precision = toRefresh->getData()->size()*2;
        RawData * toRefreshRd = new RawData(toRefresh->getMPZ(), true);
        vector<MPZ*>* zi = FHE::genZi(toRefreshRd, precision);

        ///////////// Encrypt        
        for (DataObject * ob : *toRefresh->getData()) {
            RawData * rd = static_cast<RawData*> (ob);
            FHE::asymetricEncrypt(pk2, rd->getRefreshKey(), rd);
        }

        /////////////  Decrypt
        FHE::symetricHomomorphicDecrypt(zi, toRefresh->getEncryptedSecretKey(), static_cast<RawData*> (toRefresh->getData()->front()));

        MPZ * tmpResult = new MPZ(static_cast<RawData*> (toRefresh->getData()->front())->getData());
        toRefresh->setBits(tmpResult->getData());
        toRefresh->setSize(tmpResult->getSize());
        

    } else {
        cout << "Not refreshable" << endl;
    }
}

void FHE::refresh(RawData* toRefresh) {
    
    
    if(toRefresh->getRefreshKey()->isCheated() == 1){
        FHE::symetricDecrypt(toRefresh->getRefreshKey()->getPrivateKey(), toRefresh);
        FHE::asymetricEncrypt(toRefresh->getRefreshKey()->getPublicKey(),toRefresh->getRefreshKey(), toRefresh);        
    }
}

RawData* FHE::AND(RawData* a, RawData* b) {
   
    RawData * toReturn;

    if (a->isCiphered() && b->isCiphered()) {
        unsigned int reslutNoiseLength = fmax(b->getNoiseLength(), a->getNoiseLength())*2 + 2;
        // cout << "refresh? (AND)" << endl;
        if (reslutNoiseLength >= b->getKeyLength()) {
            
            b->trace(b,OPERATION::REFRESH);
            a->trace(a,OPERATION::REFRESH);
            //cout << "refresh and" << endl;
            if (a->getRefreshKey()->getPrivateKey() == NULL && b->getRefreshKey()->getPrivateKey() == NULL) {
                FHE::fullyHomomorphicRefresh(b);
                FHE::fullyHomomorphicRefresh(a);
                
                
            } else {
                // Testing refresh with Private Key
                FHE::refresh(b);
                FHE::refresh(a);
                
            }
            
            
            return FHE::AND(a, b);
        }

        mpz_class bData(b->getData());
        mpz_class aData(a->getData());

        mpz_class andResult(bData * aData);

        toReturn = new RawData(andResult, true, b->getKeyLength(), reslutNoiseLength);
        toReturn->setRefreshKey(a->getRefreshKey());

    } else if (!a->isCiphered() && !b->isCiphered()) {
        mpz_class bData(b->getData());
        mpz_class aData(a->getData());

        mpz_class andResult(bData & aData);

        toReturn = new RawData(andResult, false);

    } else if (a->isCiphered() && !b->isCiphered()) {
        if (b->getData() == 0) {
            toReturn = new RawData(0, false, a->getKeyLength(), a->getNoiseLength());
        } else {
            //b->getData() == 1
            toReturn = new RawData(a->getData(), a->isCiphered(), a->getKeyLength(), a->getNoiseLength());
            toReturn->setRefreshKey(a->getRefreshKey());
        }
    } else {
        //!a->isCiphered() && b->isCiphered()
        if (a->getData() == 0) {
            toReturn = new RawData(0, false, b->getKeyLength(), b->getNoiseLength());
        } else {
            //a->getData() == 1
            toReturn = new RawData(b->getData(), b->isCiphered(), b->getKeyLength(), b->getNoiseLength());
            toReturn->setRefreshKey(b->getRefreshKey());
        }
    }


    b->trace(toReturn, OPERATION::AND);
    a->trace(toReturn, OPERATION::AND);


    return toReturn;

}

RawData* FHE::XOR(RawData* a, RawData* b) {

    RawData * toReturn;

    if (a->isCiphered() && b->isCiphered()) {
        unsigned int resultNoiseLength = fmax(b->getNoiseLength(), a->getNoiseLength()) + 1;
        //cout << "refresh? (XOR)" << endl;
        if (resultNoiseLength >= b->getKeyLength()) {
            
            b->trace(b,OPERATION::REFRESH);
            a->trace(a,OPERATION::REFRESH);
            
            cout << "refresh xor" << endl;
            if (a->getRefreshKey()->getPrivateKey() == NULL && b->getRefreshKey()->getPrivateKey() == NULL) {
                FHE::fullyHomomorphicRefresh(b);
                FHE::fullyHomomorphicRefresh(a);
            } else {
                // Testing refresh with Private Key
                FHE::refresh(b);
                FHE::refresh(a);
            }
            
            return FHE::XOR(a, b);
        }
        // Sum (XOR)
        mpz_class bData(b->getData());
        mpz_class aData(a->getData());

        mpz_class xorResult(bData + aData);

        toReturn = new RawData(xorResult, true, b->getKeyLength(), resultNoiseLength);
        toReturn->setRefreshKey(a->getRefreshKey());

    } else if (!a->isCiphered() && !b->isCiphered()) {
        mpz_class bData(b->getData());
        mpz_class aData(a->getData());

        mpz_class xorResult(bData ^ aData);

        toReturn = new RawData(xorResult, false);

    } else if (a->isCiphered() && !b->isCiphered()) {
        if (b->getData() == 1) {
            toReturn = new RawData(a->getData() + 1, true, a->getKeyLength(), a->getNoiseLength() + 1);
            toReturn->setRefreshKey(a->getRefreshKey());
        } else {
            //b->getData() == 0
            toReturn = new RawData(a->getData(), true, a->getKeyLength(), a->getNoiseLength());
            toReturn->setRefreshKey(a->getRefreshKey());
        }

    } else {
        //!a->isCiphered() && b->isCiphered()
        if (a->getData() == 1) {
            toReturn = new RawData(b->getData() + 1, true, b->getKeyLength(), b->getNoiseLength() + 1);
            toReturn->setRefreshKey(b->getRefreshKey());
        } else {
            //a->getData() == 0
            toReturn = new RawData(b->getData(), true, b->getKeyLength(), b->getNoiseLength());
            toReturn->setRefreshKey(b->getRefreshKey());
        }
    }

    b->trace(toReturn, OPERATION::XOR);
    a->trace(toReturn, OPERATION::XOR);

    return toReturn;
}
