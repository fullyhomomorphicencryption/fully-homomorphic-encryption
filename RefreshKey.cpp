#include "RefreshKey.h"

RefreshKey::RefreshKey(PublicKey * publicKey, PrivateKey * privateKey, int precision) {
    this->publicKey = publicKey;
    this->privateKey = privateKey;
    this->encryptedPrivateKey = NULL;
    this->precision = precision;
    this->cheated = 1;
}

RefreshKey::RefreshKey(PublicKey * publicKey, MPZ* encryptedPrivateKey, int precision) {
    this->publicKey = publicKey;
    this->privateKey = NULL;
    this->encryptedPrivateKey = encryptedPrivateKey;
    if(precision != encryptedPrivateKey->getSize()){
        // TODO trow exception
        cout << "problem size" << endl; 
    }
    this->precision = precision;
    this->cheated = 0;
}
RefreshKey::RefreshKey(){
    
}
int RefreshKey::isCheated(){
    return this->cheated;
}
void RefreshKey::setPublicKey(PublicKey * publicKey){
    this->publicKey = publicKey; 
}

MPZ* RefreshKey::getEncryptedPrivateKey() {
    return this->encryptedPrivateKey;
}

PrivateKey* RefreshKey::getPrivateKey() {
    return this->privateKey;
}

PublicKey* RefreshKey::getPublicKey() {
    return this->publicKey;
}

int RefreshKey::getPrecision(){
    return this->precision;
}