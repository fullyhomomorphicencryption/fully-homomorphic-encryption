#pragma once
#include "ComplexData.h"
#include "Pixel.h"

class Image : public ComplexData {
public:
    Image();
    Image(Image*);
    Image(int height, int width, vector<Pixel*>* pixels);

    ~Image();

    virtual vector<DataObject*>* getData();

    int getHeight() const;
    void setHeight(int height);
    
    int getWidth() const;
    void setWidth(int width);
    
    vector<Pixel*>* getPixels() const;
    void setPixels(vector<Pixel*>* pixels);

    void blur();

private:
    int height;
    int width;
    vector<Pixel*> * pixels;
};