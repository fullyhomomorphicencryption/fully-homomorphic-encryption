#include "Client.h"

#define PORT_NUMBER     5000
#define SERVER_ADDRESS  "127.0.0.1"
#define FILENAME        "receive.jpg"

int main(int argc, char **argv) {
    Client* client = new Client(SERVER_ADDRESS, PORT_NUMBER, FILENAME);
    client->receive_sequence();
    return 1;
}