#include "Server.h"

Server::Server(char* address, unsigned int port_number, char* file_to_serve) {
	this->address = address;
	this->port_number = port_number;
	this->file_to_serve = file_to_serve;

	this->stream_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(this->stream_socket < 0) {
		std::cerr << "Error while creating socket" << std::endl;
		exit(EXIT_FAILURE);
	}

	memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    inet_pton(AF_INET, (const char*) this->address, &(server_addr.sin_addr));
    server_addr.sin_port = htons(this->port_number);

    if ((bind(this->stream_socket, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))) == -1) {
        std::cerr << "Error while binding" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Server::listen_sequence() {
	int fd, remain_data, peer_socket;
	long int offset;
    int sent_bytes = 0;
    char file_size[256];
    struct stat file_stat;
    ssize_t len;
    socklen_t sock_len;

    if ((listen(this->stream_socket, 5)) == -1) {
		std::cerr << "Error on listen" << std::endl;
        exit(EXIT_FAILURE);
    }

    fd = open(this->file_to_serve, O_RDONLY);
    if (fd == -1) {
    	std::cerr << "Error on opening file" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (fstat(fd, &file_stat) < 0) {
    	std::cerr << "Error with fstat" << std::endl;
        exit(EXIT_FAILURE);
    }

    fprintf(stdout, "File Size: \n%d bytes\n", file_stat.st_size);

    sock_len = sizeof(struct sockaddr_in);

    peer_socket = accept(this->stream_socket, (struct sockaddr *)&peer_addr, &sock_len);
    if (peer_socket == -1) {
		std::cerr << "Error on accept" << std::endl;
        exit(EXIT_FAILURE);	
    }

    fprintf(stdout, "Accept peer --> %s\n", inet_ntoa(peer_addr.sin_addr));

    sprintf(file_size, "%d", file_stat.st_size);

    len = send(peer_socket, file_size, sizeof(file_size), 0);
    if (len < 0) {
    	std::cerr << "Error on sending" << std::endl;
        exit(EXIT_FAILURE);
    }

    fprintf(stdout, "Server sent %d bytes for the size\n", len);

    offset = 0;
    remain_data = file_stat.st_size;

    while (((sent_bytes = sendfile(peer_socket, fd, &offset, BUFSIZ)) > 0) && (remain_data > 0)) {
            fprintf(stdout, "1. Server sent %d bytes from file's data, offset is now : %d and remaining data = %d\n", sent_bytes, offset, remain_data);
            remain_data -= sent_bytes;
            fprintf(stdout, "2. Server sent %d bytes from file's data, offset is now : %d and remaining data = %d\n", sent_bytes, offset, remain_data);
    }
    close(peer_socket);
}

Server::~Server() {
	close(this->stream_socket);
}