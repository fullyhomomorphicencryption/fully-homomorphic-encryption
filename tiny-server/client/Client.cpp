#include "Client.h"

Client::Client(char* address, unsigned int port_number, char* file_received) {
	this->server_address = address;
	this->port_number = port_number;
	this->file_received = file_received;

	this->stream_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(this->stream_socket < 0) {
		std::cerr << "Error while creating socket" << std::endl;
		exit(EXIT_FAILURE);
	}

    memset(&remote_addr, 0, sizeof(remote_addr));
    remote_addr.sin_family = AF_INET;
    inet_pton(AF_INET, (const char*) this->server_address, &(remote_addr.sin_addr));
    remote_addr.sin_port = htons(this->port_number);

    if (connect(this->stream_socket, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr)) == -1) {
        std::cerr << "Error on connection" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Client::receive_sequence() {
    ssize_t len;
    char buffer[BUFSIZ];
    int file_size;
    FILE *received_file;
    int remain_data = 0;

    recv(this->stream_socket, buffer, BUFSIZ, 0);
    file_size = atoi(buffer);

    received_file = fopen(this->file_received, "w");
    if (received_file == NULL) {
        std::cerr << "Error on file opening" << std::endl;
        exit(EXIT_FAILURE);
    }

    remain_data = file_size;

    while (((len = recv(this->stream_socket, buffer, BUFSIZ, 0)) > 0) && (remain_data > 0))
    {
            fwrite(buffer, sizeof(char), len, received_file);
            remain_data -= len;
            fprintf(stdout, "Receive %d bytes and we hope :- %d bytes\n", len, remain_data);
    }
    fclose(received_file);
}

Client::~Client() {
	close(this->stream_socket);
}