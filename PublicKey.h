#pragma once
class RawData;
#include "RawData.h"
#include "SecuritySet.h"
class RefreshKey;
#include "RefreshKey.h"



class PublicKey{
private:
    vector<RawData *>* publicKey;
    SecuritySet * securitySet;
    
public:
    PublicKey(vector<RawData*>* publicKey, SecuritySet * securitySet);
    PublicKey(vector<RawData*>* publicKey);
    mpz_class getMpzPublicKey();
    RawData * getPublicKey();
    vector<RawData *>* getFullPublicKey();
    void setRefreshKey(RefreshKey * key);
    SecuritySet * getSecuritySet();
};