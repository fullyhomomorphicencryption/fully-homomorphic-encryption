#include <sstream>
#include "MPZ.h"
#include "FHE.h"

MPZ::MPZ() : ComplexData() {
    this->size = 0;
}

MPZ::MPZ(MPZ * mpz) : ComplexData() {
    this->setComplex(mpz->isComplex());
    this->setSize(mpz->getSize());
    this->setRefreshable(mpz->isRefreshable());
    this->bits = new vector<DataObject*>();
    RawData* toPush;
    for (int k = 0; k < mpz->getSize(); k++) {
        toPush = new RawData(static_cast<RawData*> (mpz->bits->at(k)));
        this->bits->push_back(toPush);
    }
}

MPZ::MPZ(mpz_class data) : ComplexData() {
    this->bits = new vector<DataObject*>();
    RawData* toPush;

    string dataBits = data.get_str(2);
    this->setSize(dataBits.size());
    this->setRefreshable(false);    

    for (int i = 0; i < dataBits.size(); i++) {
        mpz_class n(dataBits[dataBits.size() - 1 - i] - '0');
        toPush = new RawData(n, false);
        this->bits->push_back(toPush);
    }
}

MPZ::MPZ(RawData * rd, MPZ* refreshKey) : ComplexData() {
    this->bits = new vector<DataObject*>();
    RawData* toPush;

    string dataBits = rd->getData().get_str(2);
    this->setSize(dataBits.size());
    this->setRefreshable(true);
    this->setEncryptedSecretKey(refreshKey);

    for (int i = 0; i < dataBits.size(); i++) {
        mpz_class n(dataBits[dataBits.size() - 1 - i] - '0');
        toPush = new RawData(n, false);
        this->bits->push_back(toPush);
    }
}

MPZ::MPZ(vector<int> bits, int size) : ComplexData() {
    this->bits = new vector<DataObject*>();
    RawData* toPush;

    this->setSize(size);
    this->setRefreshable(false);

    for (int i = size-1; i >= 0; i--) {
        mpz_class n(bits[i]);
        toPush = new RawData(n, false);
        this->bits->push_back(toPush);
    }
}

MPZ::MPZ(vector<DataObject*>* bits, int size) : ComplexData() {
    this->bits = bits;
    this->setSize(size);
    this->setRefreshable(false);
}

MPZ::~MPZ() {
    for (int i = 0; i<this->bits->size(); i++) {
        if (this->bits->at(i) != NULL) {
            delete this->bits->at(i);
            this->bits->at(i) = NULL;
        }
    }
    delete this->bits;
}

vector<DataObject*>* MPZ::getData() {
    return this->bits;
}

int MPZ::getSize() const {
    return this->size;
}

void MPZ::setSize(int size) {
    if (size >= 0) {
        this->size = size;
    } else {
        // TODO : throw error.
    }
}

MPZ* MPZ::getEncryptedSecretKey(){
    return this->encryptedSecretKey;
}

void MPZ::setEncryptedSecretKey(MPZ* encryptedSecretKey){
    this->encryptedSecretKey = encryptedSecretKey;
}

bool MPZ::isRefreshable(){
    return this->refreshable;
}

void MPZ::setRefreshable(bool isRefreshable){
    this->refreshable = isRefreshable;
}


mpz_class MPZ::getMPZ() {
    RawData* bit;
    mpz_class value;
    mpz_class result;

    stringstream stringStream;
    string stringResult;


    for (int i = 0; i < this->size; i++) {

        bit = static_cast<RawData*> (this->bits->at(i));
        value = bit->getData();

        if (!bit->isCiphered()) {
            stringStream << bit->getData().get_str(2).back();
        }
    }
    stringStream >> stringResult;

    // Invert MSB & LSB
    stringResult = string ( stringResult.rbegin(), stringResult.rend() );
    
        
    result.set_str(stringResult, 2);

    return result;
}

string MPZ::toString() {
    RawData* bit;
    mpz_class value;

    stringstream stringStream;
    string stringResult;


    for (int i = 0; i < this->size; i++) {

        bit = static_cast<RawData*> (this->bits->at(i));
        value = bit->getData();

        if (!bit->isCiphered()) {
            stringStream << bit->getData().get_str(2).back();
        } else {
            cout << "err: try to print ciphered bit" << endl;
        }
    }
    stringStream >> stringResult;

    // Invert MSB & LSB
    stringResult = string ( stringResult.rbegin(), stringResult.rend() );
    


    return stringResult;
}

MPZ* MPZ::leftShift(int times) {

    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * tmp;

    for (int j = 0; j < times; j++) {
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }

    for (int i = 0; i < this->getSize(); i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }

    return new MPZ(result, this->getSize() + times);
}

MPZ* MPZ::rightShift(int times) {

    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * tmp;

    for (int i = times; i< this->getSize(); i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }

//    for (int j = 0; j < times; j++) {
//        // push a new zero
//        result->push_back(new RawData(mpz_class(0), false));
//    }

    return new MPZ(result, this->getSize() - times);
}

MPZ* MPZ::leftPadding(int times) {
    RawData * tmp;
    vector<DataObject*>* result = new vector<DataObject*>();
    for (int i = 0; i < this->getSize(); i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }
    for (int j = 0; j < times; j++) {
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }
    return new MPZ(result, this->getSize() + times);
}

MPZ* MPZ::rightPadding(int times) {
    vector<DataObject*>* result = new vector<DataObject*>();
    for (int j = 0; j < times; j++) {
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }
    
    RawData * tmp;
    for (int i = 0; i < this->getSize(); i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }
    return new MPZ(result, this->getSize() + times);
}


MPZ* MPZ::leftCrop(int times) {
    // TODO Exceptions
    RawData * tmp;
    vector<DataObject*>* result = new vector<DataObject*>();
    for (int i = 0; i < this->getSize() - times; i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }
    return new MPZ(result, this->getSize() - times);
}

MPZ* MPZ::rightCrop(int times) {
    // TODO Exceptions
    vector<DataObject*>* result = new vector<DataObject*>();    
    RawData * tmp;
    for (int i = 0; i < this->getSize()-times; i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i+1));
        result->push_back(tmp);
    }
    return new MPZ(result, this->getSize() - times);
}

MPZ* MPZ::add(MPZ* other) {
    
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * a;
    RawData * b;
    RawData * c;
    RawData * Cin;
    RawData * out;
    
    RawData * tmpa = static_cast<RawData*> (this->getData()->at(0));
    RawData * tmpb = static_cast<RawData*> (other->getData()->at(0));
    
    Cin = FHE::AND(tmpa,tmpb);
    out = FHE::XOR(tmpa, tmpb);
    result->push_back(out);
    
    int maxSize;
    int minSize;
    
    // O = this, 1 = other
    int smaller;
    
    if(this->getSize() > other->getSize()){
        maxSize = this->getSize();
        minSize = other->getSize();
        smaller = 1;
    } else {
        minSize = this->getSize();
        maxSize = other->getSize();
        smaller = 0;
    }
    
     
    for(int i=1;i< maxSize;i++){
        
        if(i >= minSize){
            if(smaller){ // other is too short
                tmpa = static_cast<RawData*> (this->getData()->at(i));
                tmpb = new RawData(mpz_class(0), false);  
            } else { // this is too short
                tmpa = new RawData(mpz_class(0), false);
                tmpb = static_cast<RawData*> (other->getData()->at(i)); 
            }
        } else {
            tmpa = static_cast<RawData*> (this->getData()->at(i));
            tmpb = static_cast<RawData*> (other->getData()->at(i));   
        }
        
        a = FHE::XOR(tmpa, tmpb);
        b = FHE::AND(tmpa, tmpb);
        c = FHE::AND(a, Cin);
        out = FHE::XOR(a, Cin);
        Cin = FHE::XOR(c, b);
        result->push_back(out);
    }
    
    result->push_back(Cin);
    
    return new MPZ(result, maxSize+1);
}

MPZ* MPZ::addWithoutCarry(MPZ* other) {
    
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * a;
    RawData * b;
    RawData * c;
    //RawData * Cin;
    RawData * out;
    
    RawData * tmpa = static_cast<RawData*> (this->getData()->at(0));
    RawData * tmpb = static_cast<RawData*> (other->getData()->at(0));
    
    //Cin = FHE::AND(tmpa,tmpb);
    out = FHE::XOR(tmpa, tmpb);
    result->push_back(out);
    
    int maxSize;
    int minSize;
    
    // O = this, 1 = other
    int smaller;
    
    if(this->getSize() > other->getSize()){
        maxSize = this->getSize();
        minSize = other->getSize();
        smaller = 1;
    } else {
        minSize = this->getSize();
        maxSize = other->getSize();
        smaller = 0;
    }
    
     
    for(int i=1;i< maxSize;i++){
        
        if(i >= minSize){
            if(smaller){ // other is too short
                tmpa = static_cast<RawData*> (this->getData()->at(i));
                tmpb = new RawData(mpz_class(0), false);  
            } else { // this is too short
                tmpa = new RawData(mpz_class(0), false);
                tmpb = static_cast<RawData*> (other->getData()->at(i)); 
            }
        } else {
            tmpa = static_cast<RawData*> (this->getData()->at(i));
            tmpb = static_cast<RawData*> (other->getData()->at(i));   
        }
        
        a = FHE::XOR(tmpa, tmpb);
        b = FHE::AND(tmpa, tmpb);
        //c = FHE::AND(a, Cin);
        out = FHE::XOR(a, b);
        //Cin = FHE::XOR(c, b);
        result->push_back(out);
    }
    
   // result->push_back(Cin);
    
    return new MPZ(result, maxSize);
}

void MPZ::setBits(vector<DataObject*>* bits){
    this->bits = bits;
}


//MPZ* MPZ::add(MPZ* other) {
//
//}
//
//
//MPZ* MPZ::leftShift(int times = 1){
//    
//    return new MPZ(result);
//}
//
//MPZ* MPZ::rightShift(int times = 1){
//    
//    return new MPZ(result);
//}