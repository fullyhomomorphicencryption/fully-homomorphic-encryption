#include "Byte.h"
#include "FHE.h"

Byte::Byte() : ComplexData() {

}

Byte::Byte(Byte * byte) : ComplexData() {
    this->setComplex(byte->isComplex());
    this->bits = new vector<DataObject*>();
    for (int k = 0; k < 8; k++) {
        DataObject * topush = new DataObject(byte->bits->at(k));
        this->bits->push_back(topush);
    }
}

Byte::Byte(char data) : ComplexData() {
    this->bits = new vector<DataObject*>();
    RawData* toPush;
    for (int k = 0; k < 8; k++) {
        mpz_class n((data & (1 << k)) >> k);
        toPush = new RawData(n, false);
        this->bits->push_back(toPush);
    }
}

Byte::Byte(int data) : ComplexData() {

    this->bits = new vector<DataObject*>();
    RawData* toPush;
    for (int k = 0; k < 8; k++) {
        mpz_class n((data & (1 << k)) >> k);
        toPush = new RawData(n, false);
        this->bits->push_back(toPush);
    }
}

Byte::Byte(vector<DataObject*>* bits) : ComplexData() {
    this->bits = bits;
}

Byte::~Byte() {
    for (int i = 0; i<this->bits->size(); i++) {
        if (this->bits->at(i) != NULL) {
            delete this->bits->at(i);
            this->bits->at(i) = NULL;
        }
    }
    delete this->bits;
}

vector<DataObject*>* Byte::getData() {
    return this->bits;
}

int Byte::getInt() {
    RawData* bit;
    mpz_class value;
    int result = 0;
    for (int i = 0; i < 8; i++) {
        bit = static_cast<RawData*> (this->bits->at(i));
        value = bit->getData();
        if(value.get_ui() % 2 == 1){
          result = result | (1 << i);   
        }

    }
    return result;
}

Byte* Byte::add(Byte* other) {

    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * a;
    RawData * b;
    RawData * c;
    RawData * Cin;
    RawData * out;

    RawData * tmpa = static_cast<RawData*> (this->getData()->at(0));
    RawData * tmpb = static_cast<RawData*> (other->getData()->at(0));

    Cin = FHE::AND(tmpa, tmpb);
    out = FHE::XOR(tmpa, tmpb);
    result->push_back(out);


    for (int i = 1; i < 8; i++) {

        tmpa = static_cast<RawData*> (this->getData()->at(i));
        tmpb = static_cast<RawData*> (other->getData()->at(i));

        a = FHE::XOR(tmpa, tmpb);
        b = FHE::AND(tmpa, tmpb);
        c = FHE::AND(a, Cin);
        out = FHE::XOR(a, Cin);
        Cin = FHE::XOR(c, b);
        result->push_back(out);
    }

    return new Byte(result);
}

Byte* Byte::leftShift(int times = 1) {

    // TODO : throw times>8
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * tmp;

    for (int j = 0; j < times; j++) {
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }


    for (int i = 0; i < 8 - times; i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }

    return new Byte(result);
}

Byte* Byte::rightShift(int times = 1) {

    // TODO : throw times>8
    vector<DataObject*>* result = new vector<DataObject*>();
    RawData * tmp;

    for (int i = times; i < 8; i++) {
        tmp = static_cast<RawData*> (this->getData()->at(i));
        result->push_back(tmp);
    }

    for (int j = 0; j < times; j++) {
        // push a new zero
        result->push_back(new RawData(mpz_class(0), false));
    }

    return new Byte(result);
}

Byte* Byte::substract(Byte* other) {
    vector<DataObject*>* result = new vector<DataObject*>();
}