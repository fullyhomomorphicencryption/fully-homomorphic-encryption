#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>


#include "MPZ.h"
#include "Integer.h"
#include "Pixel.h"
#include "Logger.h"
#include "Image.h"
#include "KeySet.h"
#include "Tools.h"
#include "FHE.h"

#include <stdio.h>

using namespace std;

int main(int argc, char** argv) {


    int modeFully = 0;

    // gen keys
    int ssVal = 2;
    if (argc >= 3 && string("security").compare(argv[1]) == 0 && argv[2] != NULL) {
        ssVal = stoi(argv[2]);
        cout << "security set to: " << ssVal << endl;

    }
    if (argc >= 2 && string("tracking").compare(argv[1]) == 0) {
        
        Logger::getInstance()->setSendData(true);
        
        SecuritySet * ss = new SecuritySet(40, 5, 5, 10);
        KeySet * ks = FHE::generateKeySet(ss);

        mpz_class cipher0 = FHE::mpzSymetricEncrypt(ks->getPrivateKey(), 0);
        //cout<<cipher0<<endl;
        mpz_class plain0 = FHE::mpzSymetricDecrypt(ks->getPrivateKey(), cipher0);
        //cout<<plain0<<endl;

        RawData * b = new RawData(0, false, 0, 0);
        RawData * bb = new RawData(0, false, 0, 1);
        Integer * i = new Integer(10);
        Integer * ii = new Integer(10);




        FHE::asymetricEncrypt(ks->getPublicKey(), ks->getRefreshKey(), b);
        FHE::symetricEncrypt(ks->getPrivateKey(), ks->getRefreshKey(), bb);
        FHE::symetricEncrypt(ks->getPrivateKey(), ks->getRefreshKey(), i);
        FHE::symetricEncrypt(ks->getPrivateKey(), ks->getRefreshKey(), ii);



        b->setTraced(true);
        i->add(ii);

        b = FHE::AND(b, bb);

        FHE::refresh(b);



        FHE::symetricDecrypt(ks->getPrivateKey(), b);


    }

    // Init security set
    SecuritySet * ss = new SecuritySet(ssVal);
    //SecuritySet * ss = new SecuritySet(30, 5, 5, 20);
    //SecuritySet * ss = new SecuritySet(50, 0, 2, 3);


    KeySet * ks = FHE::generateKeySet(ss);

    if (argc == 4 && string("keygen").compare(argv[3]) == 0) {
        //        cout << "generated keySet to file keyset.key" <<endl;
        //        ks->exportToFile("keyset.key");
        cout << "generated public key to file key.pub" << endl;
        ks->exportPk("key.pub");
        cout << "generated private key to file key.priv" << endl;
        ks->exportSk("key.priv");
    }

    if (argc == 4 && string("pubkeygen").compare(argv[3]) == 0) {
        cout << "generated public key to file key.pub" << endl;
        ks->exportPk("key.pub");
    }


    if (argc == 5 && string("import").compare(argv[3]) == 0) {
        cout << "imported keyset from " << argv[4] << endl;
        ks->importKeySet(argv[4], ss);
    }


    // Import public key
    if (argc >= 3 && string("importpub").compare(argv[1]) == 0) {
        //cout << "imported publicKey from " << argv[2] << endl;
        ks->importPubKey(argv[2], ss);
    }

    // Import public key
    if (argc >= 3 && string("importsk").compare(argv[1]) == 0) {
        //cout << "imported secretKey from " << argv[2] << endl;
        ks->importSk(argv[2]);
    }


    if (argc >= 5 && string("encrypt").compare(argv[3]) == 0 && argv[4] != NULL) {
        int toEncrypt = stoi(argv[4]);
        RawData * toEncryptRd = new RawData(mpz_class(toEncrypt), false);
        FHE::asymetricEncrypt(ks->getPublicKey(), ks->getRefreshKey(), toEncryptRd);
        cout << "Encryption of: " << toEncrypt << endl;
        cout << "Encrypted: " << toEncryptRd->getData() << endl;

        if (argc >= 6 && string("vote").compare(argv[5]) == 0) {
            cout << "send vote: " << toEncryptRd->getData() << endl;
            ostringstream stream;
            stream << "curl -d '{\"vote\": \"" << toEncryptRd->getData() << "\"}' https://fhe-display.firebaseio.com/vote/.json";
            system(stream.str().c_str());
        }

    }

    if (argc >= 5 && string("decrypt").compare(argv[3]) == 0 && argv[4] != NULL) {
        RawData * toDecryptRd = new RawData(mpz_class(argv[4]), true);
        cout << "Encrypted: " << toDecryptRd->getData() << endl;
        FHE::symetricDecrypt(ks->getPrivateKey(), toDecryptRd);
        cout << "Decrypted: " << toDecryptRd->getData() << endl;
    }

    if (argc >= 5 && string("decryptsum").compare(argv[3]) == 0 && argv[4] != NULL) {
        ifstream importFile(argv[4]);
        MPZ * mpz = new MPZ();
        string line;
        if (importFile.is_open()) {
            vector<DataObject*>* mpzBits = new vector<DataObject*>();
            int i = 0;
            while (getline(importFile, line)) {
                if (mpz_class(line) != 0) {
                    RawData * rd = new RawData(mpz_class(line), true);

                    mpzBits->push_back(rd);
                    i++;
                }
            }
            mpz->setBits(mpzBits);
            mpz->setSize(i);


            FHE::symetricDecrypt(ks->getPrivateKey(), mpz);

            cout << mpz->getMPZ() << endl;

            importFile.close();

        } else cout << "Unable to open toSum file";
    }

    if (argc >= 3 && string("count").compare(argv[1]) == 0 && argv[2] != NULL) {
        cout << "counting" << endl;


        // need to refresh
        ks->importSk("key.priv");
        ks->importPubKey("key.pub", ss);

        string line;
        ifstream importFile(argv[2]);
        if (importFile.is_open()) {
            MPZ * sum = new MPZ(mpz_class(0));

            while (getline(importFile, line)) {
                RawData * rd = new RawData(mpz_class(line), true);
                rd->setRefreshKey(new RefreshKey(ks->getPublicKey(), ks->getPrivateKey(), rd->getData().get_str(2).size()*2));

                vector<DataObject*>* vData = new vector<DataObject*>();
                vData->push_back(rd);
                MPZ * toAdd = new MPZ(vData, 1);

                sum = sum->add(toAdd);
            }

            importFile.close();

            cout << "sum: " << sum->getMPZ() << endl;

            stringstream stream;
            system(stream.str().c_str());

            stream << "curl -X PATCH -d '{\"result\": [";

            int sumSize = sum->getSize();
            for (int k = 0; k < sumSize; k++) {
                RawData * rd = static_cast<RawData*> (sum->getData()->at(k));
                stream << "\"" << rd->getData() << "\"";
                if (k < sumSize - 1) {
                    stream << ", ";
                }
            }
            stream << "]}' https://fhe-display.firebaseio.com/vote/.json";
            system(stream.str().c_str());
        } else cout << "Unable to open sum file";

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Blur an image
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if (argc >= 4 && string("blurimage").compare(argv[1]) == 0) {
        
        int w = 32;
        int h = 32;
        
        if(argv[2] != NULL && argv[3] != NULL){

            w = stoi(argv[2]);
            h = stoi(argv[3]);
        
        }
         vector<Pixel*>* pixels = new vector<Pixel*>();
        Image * picture = new Image(w, h, pixels);

        // Read file the populate pixels
        // Use the python script image-to-rgb.py
        ifstream infile("image.rgb");
        string line;

        int iter = 0;
        int r, g, b;
        while (getline(infile, line)) {
            iter++;
            if (iter == 1) {
                r = stoi(line);
            }
            if (iter == 2) {
                g = stoi(line);
            }
            if (iter == 3) {
                b = stoi(line);
                pixels->push_back(new Pixel(r, g, b));
                iter = 0;
            }
        }


        std::cout << "============== image encrypt : ==============" << std::endl;
        //FHE::symetricEncrypt(ks->getPrivateKey(), ks->getRefreshKey(), picture);

        std::cout << "============== image blur : ==============" << std::endl;
        picture->blur();

        std::cout << "============== image decrypt : ==============" << std::endl;
        //FHE::symetricDecrypt(ks->getPrivateKey(), picture);

        std::cout << "============== Test image : ==============" << std::endl;
        vector<Pixel*>* picPixels = picture->getPixels();

        ofstream myfile;
        // Use the python scriptrgb-to-image.py
        myfile.open("blured-image.rgb");

//
        for (int j = 0; j < w * h; j++) {
            myfile << picPixels->at(j)->getR()->getInt() << "\n";
            myfile << picPixels->at(j)->getG()->getInt() << "\n";
            myfile << picPixels->at(j)->getB()->getInt() << "\n";
            //std::cout << "R: " << picPixels->at(j)->getR()->getInt() << " G: " << picPixels->at(j)->getG()->getInt() << " B: " << picPixels->at(j)->getB()->getInt() << std::endl;
        }

        myfile.close();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //        FHE::asymetricEncrypt(ks, b1);
    //        FHE::asymetricEncrypt(ks, b2);
    //        Byte * sum = new Byte(0);
    //        sum = sum->add(b1);
    //        sum = sum->add(b2);
    //        FHE::symetricDecrypt(ks->getPrivateKey(), sum);
    //        cout << "sum:" << sum->getInt() << endl;


    //
    //    // Create bit to refresh
    //    RawData * rd = new RawData(mpz_class(0), false);
    //
    //    Pixel * p = new Pixel(3, 255, 4);
    //
    //    if (modeFully) {
    //        // bits just know their encrypted privateKey
    //        FHE::asymetricRefreshableEncrypt(ks, rd);
    //        FHE::asymetricRefreshableEncrypt(ks, p);
    //    } else {
    //        // bits know their privateKey
    ////        FHE::asymetricEncrypt(ks, rd);
    ////        FHE::asymetricEncrypt(ks, p);
    //        FHE::asymetricEncrypt(ks, rd);
    //    }
    //    // Encrypt bit to refresh
    //
    ////    for (int i = 0; i < 100; i++) {
    ////        FHE::fullyHomomorphicRefresh(p);
    ////    }
    //
    //    RawData* bit = static_cast<RawData*> (p->getB()->getData()->at(0));
    //    RawData* bit2 = static_cast<RawData*> (p->getR()->getData()->at(1));
    //    mpz_class value2 = bit->getData();
    //    mpz_class value = bit->getData();
    //
    //    cout << "val: " << value << endl;
    //    cout << "val2: " << value2 << endl;
    //
    //    cout << "encrypted r: " << p->getR()->getInt() << endl;
    //    cout << "encrypted g: " << p->getG()->getInt() << endl;
    //    cout << "encrypted b: " << p->getB()->getInt() << endl;
    //
    //
    //
    //    //FHE::asymetricRefreshableEncrypt(ks, rd2);
    //
    //
    //
    //    //FHE::symetricDecrypt(ks->getPrivateKey(), rd);
    //    FHE::symetricDecrypt(ks->getPrivateKey(), p);
    //    cout << "Decrypted p r :" << p->getR()->getInt() << endl;
    //    cout << "Decrypted p g :" << p->getG()->getInt() << endl;
    //    cout << "Decrypted p b :" << p->getB()->getInt() << endl;

    //cout << "done" << endl;
    return 0;
}
