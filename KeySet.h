#pragma once

#include <string>
#include "PublicKey.h"
class PrivateKey;
#include "PrivateKey.h"
#include "RefreshKey.h"

class KeySet{
private:
    PublicKey * publicKey;
    PrivateKey * privateKey;
    RefreshKey * refreshKey;
    
public:
    KeySet(PrivateKey * privateKey, PublicKey * publicKey, RefreshKey * refreshKey);
    
    PrivateKey * getPrivateKey();
    PublicKey * getPublicKey();
    RefreshKey * getRefreshKey();

    void setPrivateKey(PrivateKey* privateKey);
    void setPublicKey(PublicKey* publicKey);
    void setRefreshKey(RefreshKey* refreshKey);

    
    void exportToFile(string path);
    void exportPk(string path);
    void exportSk(string path);
    void importKeySet(string path, SecuritySet * ss);
    void importPubKey(string path, SecuritySet * ss);
    void importSk(string path);
    
};