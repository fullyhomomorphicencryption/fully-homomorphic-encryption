#include "DataObject.h"
#include "Logger.h"

DataObject::DataObject(bool complex) {
    this->complex = complex;
}



DataObject::~DataObject() {
}


bool DataObject::isComplex() {
    return this->complex;
}

void DataObject::setComplex(bool complex) {
    this->complex = complex;
}

