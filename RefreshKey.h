#pragma once

#include <iostream>

class PublicKey;
class MPZ;
#include "PublicKey.h"
#include "PrivateKey.h"
#include "MPZ.h"

using namespace std;

class RefreshKey{
private:
    PublicKey * publicKey;
    MPZ * encryptedPrivateKey;
    PrivateKey * privateKey;
    int precision;
    int cheated;
    
public:
    RefreshKey(PublicKey * publicKey, MPZ* encryptedPrivateKey, int precision);
    RefreshKey();
    RefreshKey(PublicKey * publicKey, PrivateKey * PrivateKey, int precision);
    PublicKey * getPublicKey();
    PrivateKey * getPrivateKey();
    MPZ * getEncryptedPrivateKey();
    void setPublicKey(PublicKey* publicKey);
    int getPrecision();   
    int isCheated();
};