#include "Image.h"

Image::Image() : ComplexData() {
    this->height = 0;
    this->width = 0;
    this->pixels = new vector<Pixel*>();
}

Image::Image(Image* image) : ComplexData() {
    this->height = image->getHeight();
    this->width = image->getWidth();
    this->pixels = new vector<Pixel*>();

    for (int j = 0; j < this->height; j++) {
        for (int i = 0; i < this->width; i++) {
            this->pixels->push_back(new Pixel(image->getPixels()->at(this->width * j + i)));
        }
    }
}

Image::Image(int height, int width, vector<Pixel*>* pixels) : ComplexData() {
    this->height = height;
    this->width = width;
    this->pixels = pixels;
}

Image::~Image() {

    for (int j = 0; j < this->height; j++) {
        for (int i = 0; i < this->width; i++) {
            delete this->pixels->at(this->width * j + i);
            this->pixels->at(this->width * j + i) = NULL;
        }
    }
    delete this->pixels;
}

vector<DataObject*>* Image::getData() {
    vector<DataObject*>* pixelsList = new vector<DataObject*>();
    DataObject * currentPixel;
    for (int j = 0; j < this->height; j++) {
        for (int i = 0; i < this->width; i++) {
            currentPixel = this->pixels->at(this->width * j + i);
            pixelsList->push_back(currentPixel);
        }
    }
    
    
    return pixelsList;
}

int Image::getHeight() const {
    return height;
}

void Image::setHeight(int height) {
    this->height = height;
}

int Image::getWidth() const {
    return width;
}

void Image::setWidth(int width) {
    this->width = width;
}

vector<Pixel*>* Image::getPixels() const {
    return pixels;
}

void Image::setPixels(vector<Pixel*>* pixels) {
    this->pixels = pixels;
}

void Image::blur() {

    // copy image
    Image * bluredImage = new Image(this);

    int w = this->getWidth();
    int h = this->getHeight();
    int pixelIndex;


    //       Width
    //
    //        i ->
    // H   j * * * * *
    // e   | * * * * *
    // i  \/ * * * * *
    // g     * * * * *
    // h     * * * * *
    // t
    //
    //
    // Corner coefs :
    //
    // |*1/4 | 1/4
    // | 1/4 | 1/4
    //
    // Border coefs :
    // 
    // | 1/8 | 1/8
    // |*1/4 | 1/4
    // | 1/8 | 1/8
    //
    // Middle coefs :
    //
    // | 1/16 | 1/16 | 1/16
    // | 1/16 | *1/2 | 1/16
    // | 1/16 | 1/16 | 1/16
    //
    //
    //
    //
    //

    Byte* avgR = new Byte(0);
    Byte* avgG = new Byte(0);
    Byte* avgB = new Byte(0);
    Byte* temp = new Byte(0);


    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {


            pixelIndex = w * j + i;
            Pixel* currentPixel = this->getPixels()->at(pixelIndex);
            Pixel* bluredImagecurrentPixel = bluredImage->getPixels()->at(pixelIndex);


            // Top Left Corner
            if (i == 0 && j == 0) {
                // Red
                temp = currentPixel->getR()->add(this->getPixels()->at(w * j + (i + 1))->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getR());
                // /4 => rightShift(2)
                avgR = temp->rightShift(2);

                // Green
                temp = currentPixel->getG()->add(this->getPixels()->at(w * j + (i + 1))->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getG());
                // /4 => rightShift(2)
                avgG = temp->rightShift(2);


                // Blue
                temp = currentPixel->getB()->add(this->getPixels()->at(w * j + (i + 1))->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getB());
                // /4 => rightShift(2)
                avgB = temp->rightShift(2);
            }

            // Top border
            if (i > 0 && i < w - 1 && j == 0) {

                // Red
                temp = this->getPixels()->at(w * j + i + 1)->getR()->add(this->getPixels()->at(w * (j + 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getR());
                avgR = temp->rightShift(2);

                temp = avgR->add(this->getPixels()->at(w * (j + 1) + i)->getR());
                temp = temp->rightShift(1);

                avgR = currentPixel->getR()->add(temp);
                avgR = avgR->rightShift(1);

                // Green
                temp = this->getPixels()->at(w * j + i + 1)->getG()->add(this->getPixels()->at(w * (j + 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getG());
                avgG = temp->rightShift(2);

                temp = avgG->add(this->getPixels()->at(w * (j + 1) + i)->getG());
                temp = temp->rightShift(1);

                avgG = currentPixel->getG()->add(temp);
                avgG = avgG->rightShift(1);

                // Bed
                temp = this->getPixels()->at(w * j + i + 1)->getB()->add(this->getPixels()->at(w * (j + 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getB());
                avgB = temp->rightShift(2);

                temp = avgB->add(this->getPixels()->at(w * (j + 1) + i)->getB());
                temp = temp->rightShift(1);

                avgB = currentPixel->getB()->add(temp);
                avgB = avgB->rightShift(1);



                // Used indexes:
                //  this->getPixels()->at(w*j+i+1);
                //  this->getPixels()->at(w*(j+1)+i+1);
                //  this->getPixels()->at(w*(j+1)+i);
                //  this->getPixels()->at(w*(j+1)+i-1);
                //  this->getPixels()->at(w*j+i-1);
            }

            // Top right corner
            if (i == w - 1 && j == 0) {
                // Red :
                temp = currentPixel->getR()->add(this->getPixels()->at(w * (j + 1) + i)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getR());
                // /4 => rightShift(2)
                avgR = temp->rightShift(2);

                // Green
                temp = currentPixel->getG()->add(this->getPixels()->at(w * (j + 1) + i)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getG());
                // /4 => rightShift(2)
                avgG = temp->rightShift(2);

                // Blue
                temp = currentPixel->getB()->add(this->getPixels()->at(w * (j + 1) + i)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getB());
                // /4 => rightShift(2)
                avgB = temp->rightShift(2);
            }

            // Right border
            if (i == w - 1 && j > 0 && j < h - 1) {

                // Red
                temp = this->getPixels()->at(w * (j + 1) + i)->getR()->add(this->getPixels()->at(w * (j + 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i)->getR());

                avgR = temp->rightShift(2);

                temp = avgR->add(this->getPixels()->at(w * j + i - 1)->getR());
                temp = temp->rightShift(1);

                avgR = currentPixel->getR()->add(temp);
                avgR = avgR->rightShift(1);

                // Green
                temp = this->getPixels()->at(w * (j + 1) + i)->getG()->add(this->getPixels()->at(w * (j + 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i)->getG());

                avgG = temp->rightShift(2);

                temp = avgG->add(this->getPixels()->at(w * j + i - 1)->getG());
                temp = temp->rightShift(1);

                avgG = currentPixel->getG()->add(temp);
                avgG = avgG->rightShift(1);

                // Blue
                temp = this->getPixels()->at(w * (j + 1) + i)->getB()->add(this->getPixels()->at(w * (j + 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i)->getB());

                avgB = temp->rightShift(2);

                temp = avgB->add(this->getPixels()->at(w * j + i - 1)->getB());
                temp = temp->rightShift(1);

                avgB = currentPixel->getB()->add(temp);
                avgB = avgB->rightShift(1);


                // Used indexes:
                //  this->getPixels()->at(w*(j+1)+i);
                //  this->getPixels()->at(w*(j+1)+i-1);
                //  this->getPixels()->at(w*j+i-1);
                //  this->getPixels()->at(w*(j-1)+i-1);
                //  this->getPixels()->at(w*(j-1)+i);
            }

            // Bottom right corner
            if (i == w - 1 && j == h - 1) {

                // Red
                temp = currentPixel->getR()->add(this->getPixels()->at(w * j + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i)->getR());
                // /4 => rightShift(2)
                avgR = temp->rightShift(2);

                // Green
                temp = currentPixel->getG()->add(this->getPixels()->at(w * j + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i)->getG());
                // /4 => rightShift(2)
                avgG = temp->rightShift(2);

                // Blue
                temp = currentPixel->getB()->add(this->getPixels()->at(w * j + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i)->getB());
                // /4 => rightShift(2)
                avgB = temp->rightShift(2);
            }

            // Bottom border
            if (i > 0 && i < w - 1 && j == h - 1) {

                // Red
                temp = this->getPixels()->at(w * j + i - 1)->getR()->add(this->getPixels()->at(w * (j - 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getR());

                avgR = temp->rightShift(2);

                temp = avgR->add(this->getPixels()->at(w * (j - 1) + i)->getR());
                temp = temp->rightShift(1);

                avgR = currentPixel->getR()->add(temp);
                avgR = avgR->rightShift(1);


                // Green
                temp = this->getPixels()->at(w * j + i - 1)->getG()->add(this->getPixels()->at(w * (j - 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getG());

                avgG = temp->rightShift(2);

                temp = avgG->add(this->getPixels()->at(w * (j - 1) + i)->getG());
                temp = temp->rightShift(1);

                avgG = currentPixel->getG()->add(temp);
                avgG = avgG->rightShift(1);


                // Blue
                temp = this->getPixels()->at(w * j + i - 1)->getB()->add(this->getPixels()->at(w * (j - 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getB());

                avgB = temp->rightShift(2);

                temp = avgB->add(this->getPixels()->at(w * (j - 1) + i)->getB());
                temp = temp->rightShift(1);

                avgB = currentPixel->getB()->add(temp);
                avgB = avgB->rightShift(1);

                // Used indexes :
                //  this->getPixels()->at(w*j+i-1);
                //  this->getPixels()->at(w*(j-1)+i-1);

                //  this->getPixels()->at(w*(j-1)+i);

                //  this->getPixels()->at(w*(j-1)+i+1);
                //  this->getPixels()->at(w*j+i+1);
            }

            //Bottom Left corner
            if (i == 0 && j == h - 1) {

                // Red
                temp = currentPixel->getR()->add(this->getPixels()->at(w * (j - 1) + i)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getR());
                // /4 => rightShift(2)
                avgR = temp->rightShift(2);

                // Green
                temp = currentPixel->getG()->add(this->getPixels()->at(w * (j - 1) + i)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getG());
                // /4 => rightShift(2)
                avgG = temp->rightShift(2);

                // Blue
                temp = currentPixel->getB()->add(this->getPixels()->at(w * (j - 1) + i)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getB());
                // /4 => rightShift(2)
                avgB = temp->rightShift(2);


            }

            // Left border
            if (i == 0 && j > 0 && j < h - 1) {

                // Red
                temp = this->getPixels()->at(w * (j - 1) + i)->getR()->add(this->getPixels()->at(w * (j - 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getR());
                avgR = temp->rightShift(2);

                temp = avgR->add(this->getPixels()->at(w * j + i + 1)->getR());
                temp = temp->rightShift(1);

                avgR = currentPixel->getR()->add(temp);
                avgR = avgR->rightShift(1);

                // Green
                temp = this->getPixels()->at(w * (j - 1) + i)->getG()->add(this->getPixels()->at(w * (j - 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getG());
                avgG = temp->rightShift(2);

                temp = avgG->add(this->getPixels()->at(w * j + i + 1)->getG());
                temp = temp->rightShift(1);

                avgG = currentPixel->getG()->add(temp);
                avgG = avgG->rightShift(1);


                // Blue
                temp = this->getPixels()->at(w * (j - 1) + i)->getB()->add(this->getPixels()->at(w * (j - 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getB());
                avgB = temp->rightShift(2);

                temp = avgB->add(this->getPixels()->at(w * j + i + 1)->getB());
                temp = temp->rightShift(1);

                avgB = currentPixel->getB()->add(temp);
                avgB = avgB->rightShift(1);

                // Used indexes :
                //  this->getPixels()->at(w*(j-1)+i);
                //  this->getPixels()->at(w*(j-1)+i+1);

                //  this->getPixels()->at(w*j+i+1);

                //  this->getPixels()->at(w*(j+1)+i+1);
                //  this->getPixels()->at(w*(j+1)+i);
            }

            // Middle
            if (i > 0 && i < w - 1 && j > 0 && j < h - 1) {

                // Adds all 8 neighbors,
                // divide the sum by 8,
                // sum it with current,
                // divide by 2 for final average

                // Red
                temp = this->getPixels()->at(w * (j - 1) + i)->getR()->add(this->getPixels()->at(w * (j - 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getR());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getR());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getR());
                // /8 -> >>3
                avgR = temp->rightShift(3);
                temp = currentPixel->getR()->add(avgR);
                avgR = temp->rightShift(1);

                // Green
                temp = this->getPixels()->at(w * (j - 1) + i)->getG()->add(this->getPixels()->at(w * (j - 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getG());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getG());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getG());
                // /8 -> >>3
                avgG = temp->rightShift(3);
                temp = currentPixel->getG()->add(avgG);
                avgG = temp->rightShift(1);

                // Blue
                temp = this->getPixels()->at(w * (j - 1) + i)->getB()->add(this->getPixels()->at(w * (j - 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * j + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i + 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i)->getB());
                temp = temp->add(this->getPixels()->at(w * (j + 1) + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * j + i - 1)->getB());
                temp = temp->add(this->getPixels()->at(w * (j - 1) + i - 1)->getB());
                // /8 -> >>3
                avgB = temp->rightShift(3);
                temp = currentPixel->getB()->add(avgB);
                avgB = temp->rightShift(1);
            }


            bluredImagecurrentPixel->setR(avgR);
            bluredImagecurrentPixel->setG(avgG);
            bluredImagecurrentPixel->setB(avgB);

        }
    }

    // now replace every members of this with bluredImage
    this->height = bluredImage->getHeight();
    this->width = bluredImage->getWidth();

    for (int i = 0; i < this->width; i++) {
        for (int j = 0; j < this->height; j++) {
            // TODO: delete the pixels properly
            //delete this->pixels->at(this->width * j + i);
            this->pixels->at(this->width * j + i) = bluredImage->getPixels()->at(this->width * j + i);
        }
    }

}