# README #

##Requirements
- A C++11 compiler, basically any gcc version equal or greater than 4.9
- [GMP](https://gmplib.org/)

##Several notes
###Compiling with Makefile
In order to use the compile through Makefile or cleaning project :
```
$ make; ./run
$ make clean
```
###Installing GMP :
* Download & untar [gmp-6.0.0a](https://gmplib.org/download/gmp/gmp-6.0.0a.tar.lz)
```
#!bash

./configure --enable-cxx
make
{sudo} make install
```
* use #include <gmpxx.h>
* compile using -lgmpxx

###To do
####General 
-  ~~ Issue #1 ~~
-  ~~ Encrypt and decrypt methods should return CypherBit instance ~~
-  ~~ Better pratice with noise ~~
-  ~~ Create CipherInt class (array of 32 cipherBit) and its operations ~~
-  ~~ Handling (transparent) key refreshment by calling a check noise volume method in the encrypt method ~~
-  ~~ Create wiki page (see next section) ~~
-~~ Board journal for each CypherBit (hashmap)~~
- Compiler which manage encryption, light syntax ([introduction](http://codemink.com/create-your-own-programming-language-in-c-episode-1/), [interpretor in java](http://www.codeproject.com/Articles/50377/Create-Your-Own-Programming-Language))
- Flou

- Separate class which are able to refresh, crypt and uncrypt  in three distincts class
- Settings managements

####Conception
- ~~Abstract CypherBit, AbstractCypherManager, ...~~
-~~ Implements interface Cryptable, needs to return pointer on his data representation to crypt. ~~
    ~~For example an image will return it pixels. CypherBit must implement Cryptable.~~
####RDV
#####2 fev
- Image description (pixel describtion (bit description))
- Self-description
- [Firebase](https://www.firebase.com/docs/rest/quickstart.html) database and setup a web app
   that will check in it any changes

##Wiki
In the wiki find (depends on what is asked to us for the report) :
- Explanation of the concept
- Documentation
- Light UML
- Examples