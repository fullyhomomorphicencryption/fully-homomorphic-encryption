from PIL import Image
import numpy as np

imgSize = 32 # square image size to define

rgbArray = np.zeros((imgSize,imgSize,3), 'uint8')

lines = [line.strip() for line in open('blured-image.rgb')]

for i, line in enumerate(lines):
    if(i%3 == 0):
        rgbArray[i/imgSize/3, (i/3)%imgSize, 0] = line
    if(i%3 == 1):
        rgbArray[i/imgSize/3, (i/3)%imgSize, 1] = line
    if(i%3 == 2):
        rgbArray[i/imgSize/3, (i/3)%imgSize, 2] = line


img = Image.fromarray(rgbArray)
img.save('blured-key.bmp')
