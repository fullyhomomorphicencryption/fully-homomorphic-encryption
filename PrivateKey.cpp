#include "PrivateKey.h"

PrivateKey::PrivateKey(mpz_class key, SecuritySet* securitySet) {
    this->key = key;
    this->securitySet = securitySet;
    
}
PrivateKey::PrivateKey(mpz_class key) {
    this->key = key;
   
}

mpz_class PrivateKey::getKey(){
    return this->key;
}

SecuritySet * PrivateKey::getSecuritySet(){
    return this->securitySet;
}

void PrivateKey::setKey(mpz_class key){
    this->key = key;
}
